﻿namespace com.mobiquity.packer.Models
{
	public class PackageItemModel
	{
		public int Index { get; set; }
		public decimal Weight { get; set; }
		public decimal Cost { get; set; }
	}
}
